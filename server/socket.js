const net = require('net');
const TCP_PORT = 6974

let client = null

const initConnection = () => {
    let connection = net.createConnection({ port: TCP_PORT }, () => {
      console.log('[TCP] Connected to publisher server!');
    });
    connection.on('end', data => {
      console.log('disconnected from server');
    });
    connection.on('error', error => {
        if (error && error.errno == 'ECONNRESET') {
            console.log("[TCP] Connection has been reset, retrying connection.")
            client = initConnection()
        }
    })
    return connection
}

client = initConnection() 

module.exports = {
    tcpConnection: client,
    sendPickup: (player, what) => {
        if (client) {
            client.write("*" + JSON.stringify({
                t: 0,
                p: player,
                w: what
            }))
        }
    },
    sendKill: (attacker) => {
        if (client) {
            client.write("*" + JSON.stringify({
                t: 1,
                p: attacker,
            }))
        }
    },
    sendDeath: (player) => {
        if (client) {
            client.write("*" + JSON.stringify({
                t: 2,
                p: player,
            }))
        }
    },
    sendBomb: (player) => {
        if (client) {
            client.write("*" + JSON.stringify({
                t: 3,
                p: player,
            }))
        }
    },
    sendJoin: (player) => {
        if (client) {
            client.write("*" + JSON.stringify({
                t: 4,
                p: player,
            }))
        }
    },
    sendQuit: (player) => {
        if (client) {
            client.write("*" + JSON.stringify({
                t: 5,
                p: player,
            }))
        }
    }
}