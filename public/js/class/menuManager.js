var menuManager = {pressDer: true, pressIzq: true};
menuManager.LoadContent = function(){
    this.wrapper = document.createElement("div");
    $("body").append(this.wrapper);
    $(this.wrapper).load('ui/login', (response, status, xhr) => {
        this.uiModelList = document.querySelector('#modelList')
        this.uiButton = document.querySelector('#button')
        this.uiUsername = document.querySelector('#id')
        this.uiPassword = document.querySelector('#password')
        this.uiErrorDisplay = document.querySelector('#spanError')

        this.initialized = true    
        
        for(var i = 0; i < playerManager.pj.length; i++){
            span = document.createElement("span");
            img = document.createElement("img");
            img.id = "pj_"+i;
            img.alt = playerManager.pj[i]["pj"];
            img.src = playerManager.pj[i]["src"];
            img.addEventListener("click", function(e){
                let pt = e.toElement;
                let selecteds = document.getElementsByClassName("selected");
                for(let j = 0; j < selecteds.length; j++){
                    selecteds[j].className = "";
                }
                window.location.hash = "#"+pt.id;
                pt.className = "selected";
            });
            span.append(img);
            this.uiModelList.append(span);
        }
        document.body.addEventListener("keyup", function(event) {
            event.preventDefault();
            // el boton enter 
            if (event.keyCode === 13) {
                // simula el clic en el boton
                menuManager.uiButton.click();
            }
        });
        this.uiButton.addEventListener("click", function(){
            let str = menuManager.uiUsername.value;
            let password = menuManager.uiPassword.value;

            str = str.trim();
            if(str == "")
                menuManager.uiErrorDisplay.innerHTML = "이메일이 없습니다";
            else if(/.{50}/g.test(str))
                menuManager.uiErrorDisplay.innerHTML = "이메일이 너무 길거나 짧습니다. 0-12자로 해주세요.";
            else if(/\s/g.test(str))
                menuManager.uiErrorDisplay.innerHTML = "미확인 에러";
            else{
                let persona = document.getElementsByClassName("selected");
                let pj;
                if(window.location.hash){
                    let lel = window.location.hash.substring(1, window.location.hash.length);
                    pj = document.getElementById(lel).alt;
                }else{
                    pj = "lion";
                }
                if(persona.length != 0){
                    pj = persona[0].alt;
                }
                if(animationManager.imagenes[pj]){
                    fetch(`${ENDPOINT_HOST}:8080/auth`, { 
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            email: str,
                            password: password
                        })
                    }).then(response => response.json())
                    .then(data => {
                        if (data && data.token) {
                            io.emit('user', data.token, pj);
                            buclePrincipal.screen = screenManager.screen.GAME;
                            menuManager.Destroy();
                        } else {
                            console.warn("FUCK!")
                        }
                    })
                    .catch(error => console.warn(error));
                }else{
                    console.log("뭔 에러가 나버렸다잉");
                }
            }
        });
        document.body.addEventListener("wheel", function (e) {
            menuManager.pressDer = e.wheelDelta < 0;
            menuManager.pressIzq = e.wheelDelta > 0;  
        });
    })

    if(window.location.hash){
        let lel = window.location.hash.substring(1, window.location.hash.length);
        if(document.getElementById(lel)){
            document.getElementById(lel).click();
        }
    }
}
menuManager.Update = function(){
    if (!this.initialized) { return }
     
    if(this.pressDer){
        let selecteds = document.getElementsByClassName("selected");
        if(selecteds[0]){
            let index = parseInt(selecteds[0].id.substr(3,selecteds[0].id.length));
            index+=1;
            if(document.getElementById("pj_"+index)){
                document.getElementById("pj_"+index).click();
                window.location.hash = '#pj_'+index;
            }else{
                document.getElementById("pj_0").click();
                window.location.hash = '#pj_0';
            }
        }else{
            document.getElementById("pj_0").click();
        }
        this.pressDer = false;
    }
    if(this.pressIzq){
        let selecteds = document.getElementsByClassName("selected");
        if(selecteds[0]){
            let index = parseInt(selecteds[0].id.substr(3,selecteds[0].id.length));
            index-=1;
            if(document.getElementById("pj_"+index)){
                document.getElementById("pj_"+index).click();
                window.location.hash = '#pj_'+index;
            }else{
                index = 0;
                while(document.getElementById("pj_"+index) != undefined){
                    index+=1;
                }
                index-=1;
                document.getElementById("pj_"+index).click();
                window.location.hash = '#pj_'+index;
            }
        }else{
            document.getElementById("pj_0").click();
        }
        this.pressIzq = false;
    }
}
menuManager.UnLoadContent = function(){
    this.Destroy();
}
menuManager.Destroy = function(){
    this.wrapper.remove();
}