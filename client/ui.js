const router = require('express').Router();
var path = require('path'); 
var public = '/../public'; 

router.get('/login', (request, response) => {
    response.sendFile(path.resolve(__dirname + public + '/login.html')); // si se pide / llama al index
})

module.exports = router;
